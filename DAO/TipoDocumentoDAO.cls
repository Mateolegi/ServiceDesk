VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TipoDocumentoDAO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim DB As New ConexionDB

Public Function Todos() As List
    Set Todos = New List
    
    Rs As ADODB.Recordset
    Rs = DB.ResultSet("SELECT * FROM TIPO_DOCUMENTO")
    
    While Rs.EOF = False
        TD As New TipoDocumento
        TD.IdTipoDocumento = Rs!ID_TIPO_DOCUMENTO
        TD.Nombre = Rs!Nombre
        TD.Abreviacion = Rs!Abreviacion
        Todos.Add TD
        Rs.MoveNext
    Wend
End Function
