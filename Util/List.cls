VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "List"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim liHead As ListItem
Dim Size As Integer

Function Search(ByVal varItem As Variant, ByRef liCurrent As ListItem, ByRef liPrevious As ListItem) As Boolean
    Dim fFound As Boolean
    fFound = False
    Set liPrevious = Nothing
    Set liCurrent = liHead
    Do While Not liCurrent Is Nothing
        With liCurrent
            If varItem > .Value Then
                Set liPrevious = liCurrent
                Set liCurrent = .NextItem
            Else
                Exit Do
            End If
        End With
    Loop
    ' You can't compare the value in liCurrent to the sought
    ' value unless liCurrent points to something.
    If Not liCurrent Is Nothing Then
        fFound = (liCurrent.Value = varItem)
    End If
    Search = fFound
End Function

Public Sub Add(varValue As Variant)
    Dim liNew As New ListItem
    Dim liCurrent As ListItem
    Dim liPrevious As ListItem
    liNew.Value = varValue
    ' Find where to put the new item. This function call
    ' fills in liCurrent and liPrevious.
    Call Search(varValue, liCurrent, liPrevious)
    If Not liPrevious Is Nothing Then
        Set liNew.NextItem = liPrevious.NextItem
        Set liPrevious.NextItem = liNew
    Else
        ' Inserting at the head of the list:
        ' Set the new item to point to what liHead currently
        ' points to (which might just be Nothing). Then
        ' make liHead point to the new item.
        Set liNew.NextItem = liHead
        Set liHead = liNew
    End If
End Sub

Public Function Delete(varItem As Variant) As Boolean
    Dim liCurrent As ListItem
    Dim liPrevious As ListItem
    Dim fFound As Boolean
    ' Find the item. This function call
    ' fills in liCurrent and liPrevious.
    fFound = Search(varItem, liCurrent, liPrevious)
    If fFound Then
        If Not liPrevious Is Nothing Then
             ' Deleting from the middle or end of the list.
             Set liPrevious.NextItem = liCurrent.NextItem
        Else
            ' Deleting from the head of the list.
            Set liHead = liCurrent.NextItem
        End If
    End If
    Delete = fFound
End Function

Public Sub DebugList()
    ' Print the list to the Immediate window.
    Dim liCurrent As ListItem
    Set liCurrent = liHead
    Do While Not liCurrent Is Nothing
        Debug.Print liCurrent.Value
        Set liCurrent = liCurrent.NextItem
    Loop
End Sub

Public Function Find(Index As Integer) As Variant
    If Not Index >= Size Then
        For i = 0 To Index
            
        Next
    End If
End Function

Private Sub Class_Initialize()
    Size = 0
End Sub
