VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ConexionDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim DB As ADODB.Connection
Dim Conexion As String

Private Sub Class_Initialize()
    Conexion = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\Service Desk\ServiceDesk.mdb"
    'Crear los objectos
    Set DB = New ADODB.Connection
    'Conexi�n
    DB.Open Conexion
End Sub

Public Function ResultSet(Query As String) As ADODB.Recordset
    Set ResultSet = New ADODB.Recordset
    ResultSet.Open Query, DB, adOpenDynamic, adLockOptimistic
End Function
