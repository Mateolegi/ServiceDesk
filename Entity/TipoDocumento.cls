VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TipoDocumento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private id As Integer
Private nNombre As String
Private aAbreviacion As String

Public Property Get IdTipoDocumento() As Integer
    IdTipoDocumento = id
End Property

Public Property Let IdTipoDocumento(Value As Integer)
    id = Value
End Property

Public Property Get Nombre() As String
    Nombre = nNombre
End Property

Public Property Let Nombre(Value As String)
    nNombre = Value
End Property

Public Property Get Abreviacion() As String
    Abreviacion = aAbreviacion
End Property

Public Property Let Abreviacion(Value As String)
    aAbreviacion = Value
End Property
